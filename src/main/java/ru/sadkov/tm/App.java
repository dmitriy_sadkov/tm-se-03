package ru.sadkov.tm;

import ru.sadkov.tm.menu.CommandRealization;
import ru.sadkov.tm.menu.Commands;
import ru.sadkov.tm.service.ProjectServise;
import ru.sadkov.tm.service.TaskService;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ProjectServise projectService = new ProjectServise();
        TaskService taskServise = new TaskService(projectService);
        CommandRealization commander = new CommandRealization(taskServise,projectService,scanner);
        System.out.println("*** WELCOME TO TASK MANAGER");
        System.out.println("enter HELP for command list");
        System.out.println("enter EXIT for exit");
        String command;
        do{
            command = scanner.nextLine().toLowerCase();
            switch(command){
                case Commands.PROJECTCREATE:
                case Commands.PCR:{
                    commander.createProject();
                    break;
                }
                case Commands.PROJECTREMOVE:
                case Commands.PR:{
                    commander.removeProject();
                    break;
                }
                case Commands.PROJECTLIST:
                case Commands.PL: {
                    commander.projectList();
                    break;
                }
                case Commands.PROJECTLEAR:
                case Commands.PCL:{
                    commander.projectClear();
                    break;
                }
                case Commands.TASKCREATE:
                case Commands.TCR:{
                    commander.createTask();
                    break;
                }
                case Commands.TASKREMOVE:
                case Commands.TR:{
                    commander.removeTask();
                    break;
                }
                case Commands.ALLTASK:
                case Commands.TL:{
                    commander.showTasks();
                    break;
                }
                case Commands.TASKCLEAR:
                case Commands.TCL:{
                    commander.taskClear();
                    break;
                }
                case Commands.HELP:{
                    commander.showHelp();
                    break;
                }
                case Commands.ALLTASKSFORPROJECT:
                case Commands.TFP:{
                    commander.showTasksForProject();
                    break;
                }

                default:{
                    System.out.println("[INCORRECT COMMAND]");
                }

            }
        }while (!command.equalsIgnoreCase("exit"));

    }
}

