package ru.sadkov.tm.util;

import java.util.List;

public class ShowList {
    public static void showList(List<?> list){
        for (Object o: list) {
            System.out.println(o);
        }
    }
}
