package ru.sadkov.tm.menu;

public class Commands {
    public static final String PROJECTCREATE = "project-create";
    public static final String PCR = "pcr";

    public static final String PROJECTLIST = "project-list";
    public static final String PL = "pl";

    public static final String PROJECTREMOVE = "project-remove";
    public static final String PR = "pr";

    public static final String PROJECTLEAR = "project-clear";
    public static final String PCL = "pcl";

    public static final String TASKCREATE = "task-create";
    public static final String TCR = "tcr";

    public static final String ALLTASK = "show-all-task";
    public static final String TL = "tl";

    public static final String TASKREMOVE = "task-remove";
    public static final String TR = "tr";

    public static final String TASKCLEAR = "task-clear";
    public static final String TCL = "tcl";

    public static final String ALLTASKSFORPROJECT = "tasks-for-project";
    public static final String TFP = "tfp";

    public static final String HELP = "help";

    public static final String EXIT = "EXIT";

}
