package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.RandomUUID;

import java.util.Iterator;
import java.util.List;

public class TaskService {
    private TaskRepository taskRepo = new TaskRepository();
    private ProjectServise projectServise;

    public TaskService(ProjectServise projectServise) {
        this.projectServise = projectServise;
    }

    public TaskRepository getTaskRepo() {
        return taskRepo;
    }

    public void setTaskRepo(TaskRepository taskRepo) {
        this.taskRepo = taskRepo;
    }

    public void saveTask(String taskName, String projectName) {
        if(taskName==null||taskName.isEmpty()||projectName==null||projectName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
           String projectID = projectServise.fingProjectIdByName(projectName);
            if (!projectID.equals("")) {
                taskRepo.saveTask(new Task(taskName, RandomUUID.genRandomUUID(), projectID));
                System.out.println("[OK]");
            } else {
                System.out.println("[INCORRECT PROJECT NAME]");
            }

        }
    }

    public void removeTask(String taskName) {
        if(taskName==null||taskName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            taskRepo.removeTask(taskName);
            System.out.println("[OK]");
        }
    }

    public void showTasks() {
        if(taskRepo.isEmpty()){
            System.out.println("[NO TASKS]");
        }else {
            int i =1;
            for (Task task:taskRepo.getTaskList()) {
                System.out.println(i+". "+task.getTaskName());
            }
        }
    }

    public void clearTasks() {
        taskRepo.clearTasks();
    }

    public void removeTaskForProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            return;
        } else {
            String projectId = projectServise.fingProjectIdByName(projectName);
            taskRepo.getTaskList().removeIf(task -> task.getProjectId().equals(projectId));
        }
    }
}
