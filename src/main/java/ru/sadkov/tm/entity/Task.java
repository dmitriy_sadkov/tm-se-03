package ru.sadkov.tm.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Task {
    private String taskName;
    private String taskId;
    private String description;
    private Date dateBegin;
    private Date dateEnd;
    private String projectId;

    public Task(String taskName, String taskId, String projectId) {
        this.taskName = taskName;
        this.taskId = taskId;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TASK NAME: " + taskName + "\n");
        sb.append("TASK ID: " + taskId+ "\n");
        sb.append("PROJECT ID: " + projectId+ "\n");
        sb.append("DESCRIPTION: " + description+ "\n");
        sb.append("DATE BEGIN: " + dateBegin+ "\n");
        sb.append("DATE END: "+ dateEnd+ "\n\n");
        return sb.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
